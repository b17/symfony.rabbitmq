<?php


namespace AppBundle\Command;


use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ProducerCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this->setName('app:producer');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();
        $producer = $container->get('old_sound_rabbit_mq.thumbnail_producer');

        for ($i = 0; $i < 100000; ++$i) {
            $producer->publish('wooo:' . $i);
        }
    }
}