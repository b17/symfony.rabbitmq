<?php


namespace AppBundle\Consumer;


use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class ThumbnailConsumer implements ConsumerInterface
{
    public function execute(AMQPMessage $msg)
    {
        echo $msg->body . PHP_EOL;
    }
}